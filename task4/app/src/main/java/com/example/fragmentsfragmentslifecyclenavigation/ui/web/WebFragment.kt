package com.example.fragmentsfragmentslifecyclenavigation.ui.web

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.example.fragmentsfragmentslifecyclenavigation.R
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_web.*

class WebFragment : Fragment(), BackInterface {
//    private lateinit var viewModel: WebViewModel

    companion object {
        fun newInstance() = WebFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_web, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webViewSetup()
    }

    private fun webViewSetup() {
        web_el.webViewClient = WebViewClient()
        web_el.apply {
            loadUrl("https://www.google.com/")
            settings.javaScriptEnabled = true
            settings.safeBrowsingEnabled = true
        }
    }

    override fun onBackPressed(): Boolean {
        return if (web_el.canGoBack()) {
            web_el.goBack()
            return true
        } else {
            return false
        }
    }
}
