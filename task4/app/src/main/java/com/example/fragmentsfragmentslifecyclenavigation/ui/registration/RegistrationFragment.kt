package com.example.fragmentsfragmentslifecyclenavigation.ui.registration

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.fragmentsfragmentslifecyclenavigation.R
import com.example.fragmentsfragmentslifecyclenavigation.databinding.FragmentRegistrationBinding
import com.example.fragmentsfragmentslifecyclenavigation.ui.db.MyDBManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_registration.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch

class RegistrationFragment : Fragment() {
    private lateinit var viewModel: RegistrationViewModel
    private var _binding: FragmentRegistrationBinding? = null
    private val binding get() = _binding!!

    companion object {
        fun newInstance() = RegistrationFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this)[RegistrationViewModel::class.java]
        _binding = FragmentRegistrationBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val editSignUpEmailAddress = binding.editSignUpEmailAddress
        val editSignUpPassword = binding.editSignUpPassword
        val toSignUpButton = binding.toLoginButton
        val loginButton  = binding.signUpButton
        val myDbManager = MyDBManager(this.requireContext())

        viewModel.email.observe(viewLifecycleOwner) {
            editSignUpEmailAddress.setText(it)
        }

        viewModel.password.observe(viewLifecycleOwner) {
            editSignUpPassword.setText(it)
        }

        toSignUpButton.setOnClickListener {
            findNavController().navigate(R.id.action_registration_to_login)
        }

        loginButton.setOnClickListener {
            when (true) {
                editSignUpName.text.toString() == "" -> {
                    Toast
                        .makeText(
                            activity,
                            getString(R.string.enter_name),
                            Toast.LENGTH_SHORT
                        ).show()
                }
                editSignUpSurname.text.toString() == "" -> {
                    Toast
                        .makeText(
                            activity,
                            getString(R.string.enter_surname),
                            Toast.LENGTH_SHORT
                        ).show()
                }
                editSignUpEmailAddress.text.toString() == "" -> {
                    Toast
                        .makeText(
                            activity,
                            getString(R.string.enter_email),
                            Toast.LENGTH_SHORT
                        ).show()
                }
                editSignUpPassword.text.toString() == "" -> {
                    Toast
                        .makeText(
                            activity,
                            getString(R.string.enter_password),
                            Toast.LENGTH_SHORT
                        ).show()
                }
                else -> {
                    var Name = editSignUpName.text.toString()
                    var Surname = editSignUpSurname.text.toString()
                    var Email = editSignUpEmailAddress.text.toString()
                    var Password = editSignUpPassword.text.toString()
                    GlobalScope.launch {
                        myDbManager.openDB()
                        myDbManager.addUser(Name, Surname, Email, Password)
                        myDbManager.closeDB()
                    }
                    findNavController().navigate(R.id.action_registration_to_home)
                    requireActivity()
                        .findViewById<BottomNavigationView>(R.id.nav_view)
                        .visibility = View.VISIBLE
                }
            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}