package com.example.fragmentsfragmentslifecyclenavigation.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fragmentsfragmentslifecyclenavigation.R
import com.example.fragmentsfragmentslifecyclenavigation.RecycleElement
import com.example.fragmentsfragmentslifecyclenavigation.RecycleElementAdapter
import com.example.fragmentsfragmentslifecyclenavigation.databinding.ActivityMainBinding
import com.example.fragmentsfragmentslifecyclenavigation.databinding.FragmentHomeBinding
import com.example.fragmentsfragmentslifecyclenavigation.ui.web.WebFragment
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.recycle_element.*

class HomeFragment : Fragment() {
    //private lateinit var viewModel: HomeViewModel
    private val adapters = RecycleElementAdapter()
    private val stroki = listOf(
        RecycleElement("1)Понедельник"),
        RecycleElement("2)Вторник"),
        RecycleElement("3)Среда"),
        RecycleElement("4)Четверг"),
        RecycleElement("5)Пятница"),
        RecycleElement("6)Суббота"),
        RecycleElement("7)Воскресенье"),
    )


    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        HomeRecView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = adapters
            adapters.addstroki(stroki)
        }
    }
}